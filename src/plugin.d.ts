// TODO make all optional properties required

type PlatformContentDetails = PlatformPostDetails | PlatformVideoDetails
interface IPlatformContent {
    readonly plugin_type: string

}
interface IPlatformContentDef {
    readonly id: PlatformID
    readonly name: string
    /** the array of Thumbnails is for posts */
    readonly thumbnails?: Thumbnails | Thumbnails[]
    readonly author: PlatformAuthorLink
    /** unix time */
    readonly datetime?: number
    readonly url: string
}
declare class PlatformID {
    /** The name of the platform */
    readonly platform: string
    /** The platform GUID */
    readonly pluginId: string
    /** A unique ID for the associated content or object (unique without the platform) */
    readonly value: string
    readonly claimType: number
    readonly claimFieldType: number
    constructor(platform: string, id: string, plugin_id: string, claim_type?: number, claim_field_type?: number)
}
declare class Thumbnails {
    readonly sources: Thumbnail[]
    constructor(thumbnails: Thumbnail[])
}
declare class Thumbnail {
    readonly url: string
    readonly quality: number
    constructor(url: string, quality: number)
}
declare class PlatformAuthorLink {
    readonly id: PlatformID
    readonly name: string
    readonly url: string
    readonly thumbnail?: string
    readonly subscribers?: number
    readonly membershipUrl?: string
    constructor(id: PlatformID, name: string, url: string, thumbnail?: string, subscribers?: number, membership_url?: string)
}

declare class PlatformPost implements IPlatformContent {
    readonly plugin_type: "PlatformPost"
    constructor(obj: IPlatformPostDef)
}
interface IPlatformPostDef extends IPlatformContentDef {
    /** An array of Thumbnails that correspond to the images */
    readonly thumbnails: Thumbnails[]
    /** An array of image urls that correspond to the Thumbnails */
    readonly images: string[]
    readonly description: string
}
declare class PlatformPostDetails extends PlatformPost {
    constructor(obj: IPlatformPostDetailsDef)
}
interface IPlatformPostDetailsDef extends IPlatformPostDef {
    readonly rating: IRating
    readonly textType: TextType
    readonly content: string
}

interface IPlatformNestedMediaContentDef extends IPlatformContentDef {
    readonly contentUrl: string,
    readonly contentName?: string,
    readonly contentDescription?: string,
    readonly contentProvider?: string,
    readonly contentThumbnails: Thumbnails
}
declare class PlatformNestedMediaContent {
    readonly contentUrl: string
    readonly contentName: string
    readonly contentDescription?: string
    readonly contentProvider: string
    readonly contentThumbnails: Thumbnails
    constructor(obj: IPlatformNestedMediaContentDef)
}

declare class PlatformVideo implements IPlatformContent {
    readonly plugin_type: "PlatformVideoDetails"
    readonly author: PlatformAuthorLink
    constructor(obj: IPlatformVideoDef)
}
interface IPlatformVideoDef extends IPlatformContentDef {
    readonly thumbnails: Thumbnails
    readonly duration?: number
    readonly viewCount?: number
    readonly isLive: boolean
    readonly shareUrl: string
    /** unix time */
    readonly datetime: number
}
declare class PlatformVideoDetails extends PlatformVideo {
    constructor(obj: IPlatformVideoDetailsDef)
}
interface IPlatformVideoDetailsDef extends IPlatformVideoDef {
    readonly description: string
    readonly video: IVideoSourceDescriptor
    readonly live?: IVideoSource
    readonly rating: IRating
    readonly subtitles?: ISubtitleSource[]
    readonly getContentRecommendations?: () => ContentPager
}
declare class VideoSourceDescriptor implements IVideoSourceDescriptor {
    readonly plugin_type: "MuxVideoSourceDescriptor"
    constructor(videoSourcesOrObj: IVideoSource[])
}
interface IUnMuxVideoSourceDescriptorDef {
    readonly isUnMuxed: boolean
    readonly videoSources: IVideoSource[]
}
declare class UnMuxVideoSourceDescriptor implements IVideoSourceDescriptor {
    readonly plugin_type: "UnMuxVideoSourceDescriptor"
    constructor(video_sources_or_obj: IVideoSource[], audio_sources: IAudioSource[])
    constructor(video_sources_or_obj: IUnMuxVideoSourceDescriptorDef)
}
interface IVideoSourceDescriptor {
    readonly plugin_type: string
}
interface IVideoSource {
    readonly plugin_type: string
}
interface IAudioSource {
    readonly plugin_type: string
}
interface IDrmSource {
    readonly drmLicenseUri?: string
    readonly getLicenseRequestExecutor?: () => {
        executeRequest: (url: string, headers: HTTPHeaders, method: Method, license_request_data: Uint8Array | null) => Uint8Array
        cleanup?: () => void
    }
}
declare class DashManifestRawAudioSource implements IVideoSource {
    readonly plugin_type: "DashRawAudioSource"
    constructor(obj: IDashManifestRawAudioSourceDef)
}
interface IDashManifestRawAudioSourceDef extends IDashSourceDef {
    readonly bitrate?: number
    readonly container?: string
    readonly codec?: string
    readonly manifest?: string
    readonly generate?: () => string
}
declare class DashSource implements IVideoSource {
    readonly plugin_type: "DashSource"
    constructor(obj: IDashSourceDef)
}
interface IDashSourceDef extends IDrmSource {
    readonly name?: string
    readonly duration?: number
    readonly url: string
    readonly language?: Language
    readonly requestModifier?: {
        readonly headers?: HTTPHeaders
        readonly options?: {
            /** the id of an HTTP client */
            readonly applyCookieClient: string
        }
    }
    readonly getRequestExecutor?: () => {
        executeRequest: (url: string, headers: HTTPHeaders) => Uint8Array
        cleanup?: () => void
    }
}
interface IHLSSourceDef {
    readonly name?: string
    readonly duration?: number
    readonly url: string
    readonly priority?: boolean
    readonly language?: Language
    readonly requestModifier?: {
        readonly headers?: HTTPHeaders
        readonly options?: {
            /** the id of an HTTP client */
            readonly applyCookieClient?: string
            /** the id of an HTTP client */
            readonly applyAuthClient?: string
        }
    }
}
declare class HLSSource implements IVideoSource {
    readonly plugin_type: "HLSSource"
    constructor(obj: IHLSSourceDef)
}
interface IVideoUrlSourceDef extends IDrmSource {
    readonly width: number
    readonly height: number
    readonly container: string
    readonly codec: string
    readonly name: string
    readonly bitrate: number
    readonly duration: number
    readonly url: string
    readonly requestModifier?: {
        readonly headers?: HTTPHeaders
        readonly options?: {
            /** the id of an HTTP client */
            readonly applyCookieClient?: string
            /** the id of an HTTP client */
            readonly applyAuthClient?: string
        }
    }
    readonly getRequestExecutor?: () => {
        executeRequest: (url: string) => Uint8Array
        cleanup?: () => void
    }
}
declare class VideoUrlSource implements IVideoSource {
    readonly plugin_type: "VideoUrlSource"
    constructor(obj: IVideoUrlSourceDef)

    getRequestModifier(this: VideoUrlSource): RequestModifier | undefined
}
interface IAudioUrlSourceDef extends IDrmSource {
    readonly name: string
    readonly bitrate: number
    readonly container: string
    readonly codec: string
    readonly duration: number
    readonly url: string
    readonly language: Language
    readonly requestModifier?: {
        readonly headers: HTTPHeaders
    }
}
declare class AudioUrlSource implements IAudioSource {
    readonly plugin_type: "AudioUrlSource"
    constructor(obj: IAudioUrlSourceDef)

    getRequestModifier(this: AudioUrlSource): RequestModifier | undefined
}
interface IRequest {
    readonly url: string
    readonly headers: HTTPHeaders
}
interface IRequestModifierDef {
    readonly allowByteSkip: boolean
}
declare class RequestModifier {
    constructor(obj: IRequestModifierDef)

    modifyRequest(this: RequestModifier, url: string, headers: HTTPHeaders): IRequest
}
interface IVideoUrlRangeSourceDef extends IVideoUrlSourceDef {
    readonly itagId: number
    readonly initStart: number
    readonly initEnd: number
    readonly indexStart: number
    readonly indexEnd: number
}
declare class VideoUrlRangeSource extends VideoUrlSource {
    constructor(obj: IVideoUrlRangeSourceDef)
}
interface IAudioUrlRangeSourceDef extends IAudioUrlSourceDef {
    readonly itagId: number
    readonly initStart: number
    readonly initEnd: number
    readonly indexStart: number
    readonly indexEnd: number
    readonly audioChannels: number
}
declare class AudioUrlRangeSource extends AudioUrlSource {
    constructor(obj: IAudioUrlRangeSourceDef)
}

interface ISubtitleSource {
    readonly name: string
    readonly url: string
    readonly format: "text/vtt"
    readonly getSubtitles?: () => string
}

interface IRating {
    readonly type: 1 | 2 | 3
}
declare class RatingLikes implements IRating {
    readonly type: 1
    constructor(likes: number)
}
declare class RatingLikesDislikes implements IRating {
    readonly type: 2
    constructor(likes: number, dislikes: number)
}
declare class RatingScaler implements IRating {
    readonly type: 3
    constructor(value: number)
}

interface IPlatformChannelDef {
    readonly id: PlatformID
    readonly name: string
    readonly thumbnail: string
    readonly banner?: string
    readonly subscribers?: number
    readonly description?: string
    readonly url: string
    readonly urlAlternatives?: string[]
    readonly links?: Readonly<Record<string, string>>
}
declare class PlatformChannel {
    readonly plugin_type: "PlatformChannel"
    readonly subscribers: number
    constructor(obj: IPlatformChannelDef)
}

interface ICommentDef<T extends Readonly<Record<string, string>>> {
    /** The content URL to which this comment pertains */
    readonly contextUrl: string
    readonly author: PlatformAuthorLink
    readonly message: string
    readonly rating: IRating
    readonly date: number
    readonly replyCount: number
    /** Context to reference in getSubComments */
    readonly context: T
}
declare class PlatformComment<T extends Readonly<Record<string, string>>> implements ICommentDef<T>, IPlatformContent {
    readonly plugin_type: "Comment"
    readonly contextUrl: string
    readonly author: PlatformAuthorLink
    readonly message: string
    readonly rating: IRating
    readonly date: number
    readonly replyCount: number
    /** Context passed to the constructor during creation */
    readonly context: T
    constructor(obj: ICommentDef<T>)
}

declare class ContentPager {
    results: IPlatformContent[]
    hasMore: boolean
    constructor(results: IPlatformContent[], hasMore: boolean)

    hasMorePagers(this: ContentPager): boolean
    /**
     * Should return self
     * @param this 
     */
    nextPage(this: ContentPager): ContentPager
}
declare class ChannelPager {
    results: PlatformChannel[]
    hasMore: boolean
    constructor(results: PlatformChannel[], hasMore: boolean)

    hasMorePagers(this: ChannelPager): boolean
    /**
     * Should return self
     * @param this 
     */
    nextPage(this: ChannelPager): ChannelPager
}
declare class VideoPager {
    results: PlatformVideo[]
    hasMore: boolean
    constructor(results: PlatformVideo[], has_more: boolean)

    hasMorePagers(this: VideoPager): boolean
    /**
     * Should return self
     * @param this 
     */
    nextPage(this: VideoPager): VideoPager
}
declare class CommentPager<T extends Readonly<Record<string, string>>> {
    results: PlatformComment<T>[]
    hasMore: boolean
    constructor(results: PlatformComment<T>[], has_more: boolean)

    hasMorePagers(this: CommentPager<T>): boolean
    /**
     * Should return self
     * @param this 
     */
    nextPage(this: CommentPager<T>): CommentPager<T>
}
declare class PlaylistPager {
    results: PlatformPlaylist[]
    hasMore: boolean
    constructor(results: PlatformPlaylist[], has_more: boolean)

    hasMorePagers(this: PlaylistPager): boolean
    /**
     * Should return self
     * @param this 
     */
    nextPage(this: PlaylistPager): PlaylistPager
}

declare class ResultCapabilities<S extends string, Types extends FeedType> {
    readonly types: Types[]
    readonly sorts: Order[]
    readonly filters: FilterGroup<S>[]
    /**
     * 
     * @param types The available search sources/pagers (corresponds to a single web request)
     * @param sorts The sorting/ordering options available
     * @param filters The filtering options available
     */
    constructor(types: Types[], sorts: Order[], filters: FilterGroup<S>[])
}
declare class FilterGroup<S extends string> {
    readonly name: string
    readonly filters: FilterCapability[]
    readonly isMultiSelect: boolean
    readonly id: S
    /**
     * 
     * @param name The display name of the filter
     * @param filters The filter options
     * @param isMultiSelect Whether the user can select multiple options as the same time
     * @param id The internal id of the filter
     */
    constructor(name: string, filters: FilterCapability[], is_multi_select: boolean, id: S)
}
declare class FilterCapability {
    name: string
    value: string
    id: string
    /**
     * 
     * @param name The display name of the filter option
     * @param value The internal filter option
     * @param id The same as the name parameter
     */
    constructor(name: string, value: string, id: string)
}

interface IPlatformPlaylistDef extends IPlatformContentDef {
    readonly videoCount?: number
    /** IPlatformPlaylistDetailsDef only shows this on the import playlists screen */
    readonly thumbnail?: string
}
declare class PlatformPlaylist implements IPlatformContent {
    readonly plugin_type: "PlatformPlaylist"
    constructor(obj: IPlatformPlaylistDef)
}
interface IPlatformPlaylistDetailsDef extends IPlatformPlaylistDef {
    readonly contents: VideoPager
}
declare class PlatformPlaylistDetails implements IPlatformContent {
    readonly plugin_type: "PlatformPlaylistDetails"
    contents: VideoPager
    constructor(obj: IPlatformPlaylistDetailsDef)
}

declare class PlaybackTracker {
    private nextRequest: number
    /**
     * 
     * @param interval the number of milliseconds between onProgress calls
     */
    constructor(interval: number)

    onInit(seconds: number): void
    onProgress(seconds: number, is_playing: boolean): void
    /**
     * Called when exiting the player
     */
    onConcluded(): void
}

/** The available search sources/pagers (corresponds to a single web request) */
type FeedType = GaryjayTypes["Feed"][keyof GaryjayTypes["Feed"]]
/** The possible sorting/ordering options */
type Order = GaryjayTypes["Order"][keyof GaryjayTypes["Order"]]
type ChapterType = GaryjayTypes["Chapter"][keyof GaryjayTypes["Chapter"]]
type TextType = GaryjayTypes["Text"][keyof GaryjayTypes["Text"]]

type GaryjayTypes = {
    readonly Source: {
        readonly Dash: "DASH"
        readonly HLS: "HLS"
        readonly STATIC: "Static"
    },
    readonly Feed: {
        readonly Videos: "VIDEOS"
        readonly Live: "LIVE"
        readonly Mixed: "MIXED"
        readonly Shorts: "SHORTS"
    },
    readonly Order: {
        Chronological: "Latest releases" | "最新发布" | "CHRONOLOGICAL"
        Views: "Most played" | "最多播放"
        Favorites: "Most favorited" | "最多收藏"
        Oldest: "Oldest"
    },
    readonly Date: {
        readonly LastHour: "LAST_HOUR"
        readonly Today: "TODAY"
        readonly LastWeek: "LAST_WEEK"
        readonly LastMonth: "LAST_MONTH"
        readonly LastYear: "LAST_YEAR"
    },
    readonly Duration: {
        readonly Short: "SHORT"
        readonly Medium: "MEDIUM"
        readonly Long: "LONG"
    },
    readonly Text: {
        readonly RAW: 0
        readonly HTML: 1
        readonly MARKUP: 2
    },
    readonly Chapter: {
        readonly NORMAL: 0
        readonly SKIPPABLE: 5
        readonly SKIP: 6
        readonly SKIPONCE: 7
    }
}

type Language = LanguageOptions[keyof LanguageOptions]

type LanguageOptions = {
    readonly UNKNOWN: "Unknown"
    readonly ARABIC: "ar"
    readonly SPANISH: "es"
    readonly FRENCH: "fr"
    readonly HINDI: "hi"
    readonly INDONESIAN: "id"
    readonly KOREAN: "ko"
    readonly PORTUGUESE: "pt"
    // readonly PORTBRAZIL: "pt"
    readonly RUSSIAN: "ru"
    readonly THAI: "th"
    readonly TURKISH: "tr"
    readonly VIETNAMESE: "vi"
    readonly ENGLISH: "en"
    readonly GERMAN: "de"
}

type Source<T extends Readonly<Record<string, string>>, S extends string, ChannelTypes extends FeedType, SearchTypes extends FeedType, ChannelSearchTypes extends FeedType, Settings> = {
    enable?: (config: SourceConfig, settings: Settings, saved_state?: string | null) => void
    disable?: () => void
    saveState?: () => string

    getHome?: (continuationToken?: unknown) => ContentPager

    searchSuggestions?: (query: string) => string[]
    search?: (query: string, type: SearchTypes | null, order: Order | null, filters: FilterQuery<S> | null) => ContentPager
    getSearchCapabilities?: () => ResultCapabilities<S, SearchTypes>

    isContentDetailsUrl?: (url: string) => boolean
    getContentDetails?: (url: string) => PlatformContentDetails
    getContentRecommendations?: (url: string) => ContentPager

    isChannelUrl?: (url: string) => boolean
    getChannel?: (url: string) => PlatformChannel

    getChannelContents?: (url: string, type: ChannelTypes | null, order: Order | null, filters: FilterQuery<S> | null) => ContentPager
    getChannelCapabilities?: () => ResultCapabilities<S, ChannelTypes>

    searchChannelContents?: (channelUrl: string, query: string, type: ChannelSearchTypes | null, order: Order | null, filters: FilterQuery<S> | null) => ContentPager
    getSearchChannelContentsCapabilities?: () => ResultCapabilities<S, ChannelSearchTypes>

    searchChannels?: (query: string) => ChannelPager

    getComments?: (url: string) => CommentPager<T>
    getSubComments?: (comment: PlatformComment<T>) => CommentPager<T>

    isPlaylistUrl?: (url: string) => boolean
    getPlaylist?: (url: string) => PlatformPlaylistDetails
    searchPlaylists?: (query: string) => PlaylistPager
    getChannelPlaylists?: (url: string) => PlaylistPager

    getLiveChatWindow?: (url: string) => { url: string, removeElements: string[] }

    /**
     * 
     * @param url 
     * @returns a {@link PlaybackTracker} for the given content {@link url} or null if playback should not be tracked
     */
    getPlaybackTracker?: (url: string) => PlaybackTracker | null

    /**
     * Returns an array of channel urls
     */
    getUserSubscriptions?: () => string[]
    /**
     * Returns an array of playlist urls
     */
    getUserPlaylists?: () => string[]
}

type FilterQuery<S extends string> = Readonly<Record<S, string[]>>

type SourceConfig = {
    readonly id: string
}

declare class ScriptException extends Error {
    /**
     * 
     * @param type The type of exception (interpreted as msg if nothing is passed for msg)
     * @param msg The exception message
     */
    constructor(type: string, msg?: string)
}
declare class UnavailableException extends ScriptException {
    constructor(msg: string)
}
declare class LoginRequiredException extends ScriptException {
    constructor(msg: string)
}
declare class CaptchaRequiredException extends Error {
    constructor(url: string, body?: string)
}

declare function log(...message: unknown[]): void

declare function HTTP_GET(url: string, headers: HTTPHeaders, use_auth_client: boolean): BridgeHttpResponse<string>
declare function HTTP_GET(url: string, headers: HTTPHeaders, use_auth_client: boolean, use_byte_response: true): BridgeHttpResponse<Uint8Array>

declare function HTTP_POST(url: string, body: string | Uint8Array, headers: HTTPHeaders, use_auth_client: boolean): BridgeHttpResponse<string>
declare function HTTP_POST(url: string, body: string | Uint8Array, headers: HTTPHeaders, use_auth_client: boolean, use_byte_response: true): BridgeHttpResponse<Uint8Array>

declare function CUSTOM_HTTP_GET(url: string, headers: HTTPHeaders, return_type: 0): BridgeHttpResponse<string>
declare function CUSTOM_HTTP_GET(url: string, headers: HTTPHeaders, return_type: 1): BridgeHttpResponse<Uint8Array>

type HTTP = {
    readonly GET: typeof HTTP_GET
    readonly POST: typeof HTTP_POST
    readonly request: (method: MethodWithoutBody, url: string, headers: HTTPHeaders, use_auth_client: boolean) => BridgeHttpResponse<string>
    readonly requestWithBody: (method: MethodWithBody, url: string, body: string | Uint8Array, headers: HTTPHeaders, use_auth_client: boolean) => BridgeHttpResponse<string>
    readonly batch: () => BatchBuilder
    readonly socket: (url: string, headers: HTTPHeaders, use_auth_client: boolean) => SocketResult
    readonly newClient: (with_auth: boolean) => HTTPClient
    /** must not be called more than once */
    readonly getDefaultClient: (with_auth: boolean) => HTTPClient
}

type MethodWithoutBody = "GET" | "DELETE" | "HEAD"
type MethodWithBody = "PUT" | "PATCH" | "POST" | "DELETE"
type Method = MethodWithoutBody | MethodWithBody

type HTTPHeaders = Readonly<Record<string, string>>
type HTTPResponseHeaders = Readonly<Record<string, string[]>>

declare const utility: {
    readonly toBase64: (byte_array: Uint8Array) => string
    readonly fromBase64: (encoded_string: string) => Uint8Array
    readonly md5String: (str: string) => string
}

declare const http: HTTP
type HTTPClient = {
    readonly GET: typeof CUSTOM_HTTP_GET
    readonly setDefaultHeaders: (default_headers: HTTPHeaders) => HTTPClient
    readonly setDoApplyCookies: (apply: boolean) => HTTPClient
    readonly setDoUpdateCookies: (update: boolean) => HTTPClient
    readonly setDoAllowNewCookies: (allow: boolean) => HTTPClient
    readonly clientId?: string
}
type BridgeHttpResponse<T> = {
    readonly body: T
    readonly code: 200 | 404 | 403 | 400
    readonly headers: HTTPResponseHeaders
    readonly url: string
    readonly isOk: boolean
}
type SocketResult = {
    readonly isOpen: boolean
    readonly connect: (socketObj: SocketObject) => void
    readonly send: (msg: string) => void
    readonly close: () => void
}
type SocketObject = {
    readonly open: () => void
    readonly message: (msg: string) => void
    readonly closing: (code: number, reason: string) => void
    readonly closed: (code: number, reason: string) => void
    readonly failure: (exception: string) => void
}
type BatchBuilder = {
    readonly GET: (url: string, headers: HTTPHeaders, use_auth_client: boolean) => BatchBuilder
    readonly POST: (url: string, body: string, headers: HTTPHeaders, use_auth_client: boolean) => BatchBuilder
    readonly execute: () => BridgeHttpResponse<string>[]
}

declare const domParser: GrayjayDOMParser
type GrayjayDOMParser = {
    readonly parseFromString: (html: string) => DOMNode
}
type DOMNode = {
    readonly querySelector: (selectors: string) => DOMNode | undefined
    readonly querySelectorAll: (selectors: string) => DOMNode[]
    readonly getElementById: (element_id: string) => DOMNode | undefined
    readonly getElementsByClassName: (class_name: string) => DOMNode[]
    readonly getElementsByName: (element_name: string) => DOMNode[]
    readonly getAttribute: (qualified_name: string) => string
    readonly firstChild: DOMNode | undefined
    readonly text: string
}

declare const bridge: {
    readonly isLoggedIn: () => boolean
    readonly toast: (message: string) => void
    readonly throwTest: (message: string) => void
}

declare const plugin: {
    /** The running plugin config.json values */
    readonly config: SourceConfig
    readonly settings: unknown
}

declare const IS_TESTING: boolean
declare const Type: GaryjayTypes
declare const Language: LanguageOptions
declare const source: Source<Readonly<Record<string, string>>, string, FeedType, FeedType, FeedType, unknown>
